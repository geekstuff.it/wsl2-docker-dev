# wsl-docker-dev\ubuntu-20.04

The purpose of this repo is to regularly rebuild a WSL2 image from Ubuntu 20.04,
with docker engine, git, ssh, gpg and VSCode dependencies included.

This image can run in parallel without any conflict with other windows docker solutions
like Docker Desktop and Rancher Desktop.

See the [wsl-image](./wsl-image) folder for the simple Dockerfile and scripts that builds this.

## What it includes

- docker engine
  - installed with linux instructions (apt-get install docker)
- ssh-agent
  - the one that comes with linux
  - including an .ssh/config to start with
- gpg-agent
  - needs a windows part to make the dialog prompt available in WSL.
  - for now the import setup will try to autodetect pinentry file in specific /mnt/c places
- .gitconfig
  - with sensible defaults to start with, and plenty of aliases


All these services are launched the first time the linux user logs in.

WSL recently enabled systemd integration which is enabled in this distro, but it is not yet
fully working so some services are still started upon user logging in the first time.

Not sure if that systemd integration is problematic for non-updated windows versions.

## How to use

For a shorter version, see the latest [Release](https://gitlab.com/geekstuff.it/wsl2/docker-dev/ubuntu-20.04/-/releases)
description.

Assuming you choose to import the WSL distribution name `ubuntu-dev`:

- Ensure WSL2 is setup and working.
- If you want to sign your git commits, install [Gpg4win](https://www.gpg4win.org/) before.
  - No need for Kleopatra or anything but installation. Our WSL distribution
    will detect and use the `pinentry.exe` application installed along.
- Open a windows terminal
- Download a built `.tar.gz` image from the [package registry](https://gitlab.com/geekstuff.it/wsl2/docker-dev/ubuntu-20.04/-/packages)
  of this project.
- Import WSL distro
  - `wsl --import ubuntu-dev .\Documents\wsl-ubuntu-dev .\Downloads\wsl-docker-ubuntu2004-v0.1.tar.gz`
  - The `wsl-ubuntu-dev` folder above is where Windows will store the files for the distro.
- Start and go through post-import-setup:
  - `wsl -d ubuntu-dev post-import-setup`
  - A few remaining steps will be shown there.

NOTES:
- You can choose any name for the distribution as long as it does not conflict,
  with an already existing distribution on your Windows.

## Launch VSCode in WSL mode

- Have VSCode installed
- Install the [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension
- Start shell inside your new WSL distro
- Run `code -n ~/`

With the extension installed, the bottom left green rectangle in VSCode can also be used
to switch in which environment VSCode runs in. Your windows host, a specific WSL distro, etc.

## Test out VSCode dev containers

VSCode dev containers uses the Remote Development extension, and lets you define
a containerized development environment for a project.

Let's use a Rust cli hello world app to test this out:

- Start shell inside your new WSL distro
- `git clone https://gitlab.com/geekstuff.it/examples/rust-cli.git`
- `code -n rust-cli/`
- In VSCode, a dialog box should appear to "Reopen in container"
  - If those dialog boxes are muted, you can hit F1 and type "Reopen in container"
- Once ready, open a terminal inside VSCode
  - Ctrl+backtick or F1 and search "new terminal"
- Open file `src/main.rs` to see the hello part
- Run `make`

## What else can you do in it

Pretty much anything you think of!

For a multi-component project based on containers, you can fully use docker-compose for example.

If you want a **local Kubernetes cluster** and don't need any Windows specific integration,
you can 100% follow the Linux instructions of any local Kubernetes installation method. For
example running [Rancher in local docker mode](https://docs.ranchermanager.rancher.io/v2.5/pages-for-subheaders/rancher-on-a-single-node-with-docker) or
[KinD](https://kind.sigs.k8s.io/docs/user/quick-start/) works just fine.

The main downside at the moment is the lack of full systemd integration. Anything leveraging that
requires a little more work. At a small scale, you can for example use the `~/.profile` file and
replace the same method this project uses to run the docker, ssh-agent and gpg-agent services
[upon first user login](https://gitlab.com/geekstuff.it/wsl2/docker-dev/ubuntu-20.04/-/blob/main/wsl-image/scripts/userprofile.sh).

## How to cleanup an imported distro

Using the same example distro name above:

- `wsl --terminate ubuntu-dev`
- `wsl --unregister ubuntu-dev`
- `rm .\Documents\wsl-ubuntu-dev`

## FAQ

Can I re-use my ssh key instead of having a new one?
> Yes but every single git registries supports multiple ssh keys, so there are no valid
> reason to re-use the same one in many places. Although if it's the one from your windows
> environment, that could be different. You can also create a symlink to it from within WSL.

Same question for gpg?
> Same answer for the moment.

Am I am able to get updates if the image here is updated?
> Not at the moment but there are options that could help in that regard.
> As for your own ubuntu upgrades, they work just as usual. No changes to them.

Systemd is enabled but not working?
> Investigation was not done yet as to why since the feature itself in WSL is very new.

Why not the Windows SSH agent service?
> This was initially the choice but that service is NOT the open source version as it name tends
> to imply. For instance it ignores keys loading TTL, and saves ssh keys passphrases forever in
> Windows registry, beating the purpose of ssh key passphrases entirely.
