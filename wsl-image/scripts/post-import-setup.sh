#!/bin/sh

# Exit on any failure
set -e

# Ensure root user
if [ $(id -u) -ne 0 ]; then
    echo "This script must be run with sudo"
    exit 1
fi

if test -e /root/.post-import-completed; then
    echo "post-import was already completed. cannot run twice."
    exit 1
fi

tmpUser="wsluser"

# Request full name
printf "%s" "Enter full name (for git and linux user): "
read fullname

# Request email
printf "%s" "Enter email (for git): "
read email

# Request username
printf "%s" "Enter desired username: "
read username

# Rename user
echo "- replace user and group $tmpUser with $username..."
usermod -l "$username" -d "/home/$username" -m "$tmpUser"
usermod -c "$fullname" "$username"
groupmod -n "$username" "$tmpUser"
usermod -aG sudo $username

# passwd
echo "- set password for your linux user account (ex: for sudo purposes)"
passwd $username

# gitconfig
echo "- configure git name and email..."
git config --global user.name "$fullname"
git config --global user.email "$email"

# ssh key
echo "- generate new ed25519 ssh key. you will need to choose a passphrase."
mkdir -p /home/$username/.ssh
ssh-keygen -t ed25519 -f /home/$username/.ssh/id_ed25519 -C $username@$(hostname).${WSL_DISTRO_NAME}
chown -R $username:$username /home/$username/.ssh
chmod -R go-rwx /home/$username/.ssh
echo "- Public key:"
cat /home/$username/.ssh/id_ed25519.pub

# sudoers
echo "- adjust sudoers"
sed -i "s/$tmpUser/$username/g" /etc/sudoers.d/sudouser

# user ssh config
sed -i "s/$tmpUser/$username/g" /home/$username/.ssh/config

# Gpg agent setup
echo "- checking gpg"
GPGREADY=0
PINENTRY=""
if test -e "/mnt/c/Program Files (x86)/Gpg4win/bin/pinentry.exe"; then
    GPGREADY=1
    PINENTRY="/mnt/c/Program Files (x86)/Gpg4win/bin/pinentry.exe"
elif test -e "/mnt/c/Program Files (x86)/GnuPG/bin/pinentry-basic.exe"; then
    GPGREADY=1
    PINENTRY="/mnt/c/Program Files (x86)/GnuPG/bin/pinentry-basic.exe"
fi
if [ $GPGREADY -eq 0 ]; then
    echo "No pinentry found. If that's not intended, install https://www.gpg4win.org/ and repeat this."
else
    echo "pinentry-program $PINENTRY" > /home/$username/.gnupg/gpg-agent.conf
    chown -R $username:$username /home/$username/.gnupg
    chmod -R go-rwx /home/$username/.gnupg
    echo "Gpg setup completed. When import is done and you are in new WSL,"
    echo "see following link to create a gpg key and setup git to use it:"
    echo "https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#configure-commit-signing"
    echo ""
    echo "For a next level Gpg setup using a master key and multiple subkeys, use this link:"
    echo "https://insight.o-o.studio/article/setting-up-gpg.html"
fi

# wsl.conf
echo "- update /etc/wsl.conf"
sed -i "s/default = root/default = $username/" /etc/wsl.conf
sed -i "s/# hostname = custom-hostname/hostname = $WSL_DISTRO_NAME/" /etc/wsl.conf

# Completed!
touch /root/.post-import-completed
echo "post-import completed! complete following steps to start using your distro:"
echo ""
echo "- \`wsl --terminate $WSL_DISTRO_NAME\` to stop the WSL distro"
echo "- \`wsl --setdefault $WSL_DISTRO_NAME\` optional step to set distro as the default"
echo "- \`wsl ~ -d $WSL_DISTRO_NAME\` to start up the distro in linux home folder"

