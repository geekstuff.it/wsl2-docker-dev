#!/bin/sh

cat <<'PROFILE' >> /home/$WSL_USER/.profile
# Testing out systemd integration

# ensure docker
# Note: systemd integration is not working in our setup at the moment.
#       this looks green:
#       systemctl list-unit-files --type=service
#       but service does not start
if ! sudo /usr/sbin/service docker status > /dev/null; then
  sudo /usr/sbin/service docker start > /dev/null
  echo "docker started"
fi

# ensure ssh-agent
check-ssh-agent() {
  [ -S "$SSH_AUTH_SOCK" ] && { ssh-add -l >& /dev/null || [ $? -ne 2 ]; }
}
check-ssh-agent || export SSH_AUTH_SOCK=~/.ssh/agent.sock
check-ssh-agent || { rm -f $SSH_AUTH_SOCK; eval "$(ssh-agent -s -a $SSH_AUTH_SOCK)" > /dev/null; echo "ssh-agent started"; }

# ensure gpg-agent
if test -e ~/.gnupg && ! pidof gpg-agent >& /dev/null; then
  gpg-agent --homedir ~/.gnupg --daemon 2>/dev/null
  echo "gpg-agent started"
fi

# inform user if key is loaded
if ! ssh-add -l 1>/dev/null 2>/dev/null; then
  echo "no ssh keys loaded. Run \`ssh-add\` to load your default keys."
fi
PROFILE
