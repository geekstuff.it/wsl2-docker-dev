#!make

.PHONY: info
info:
	@echo "Welcome to a generic Devcontainer"
	@echo ""
	@echo "Things to try:"
	@echo "  make template-diff         # Show difference between with container template"
	@echo ""
	@echo "Devcontainer features:"
	@echo "  - Use TAB to auto-complete or discover make commands"
	@echo "  - Within a devcontainer, you can use VSCode to browse other files in the container:"
	@echo "    'code \$$DC_LIB'"
