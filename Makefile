#!make

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory
PACKAGE_DIR ?= packages
DOCKER_IMAGE ?= wsl-docker-test
PACKAGE_NAME ?= ${DOCKER_IMAGE}

all: info

include .make/info.mk
#include ${DC_LIB_MAKE}/template.mk

.PHONY: wsl-img-build
wsl-img-build:
	docker build -t ${DOCKER_IMAGE} -f wsl-image/Dockerfile ./wsl-image/
	@echo "Built image: [${DOCKER_IMAGE}]"

.PHONY: wsl-img-package
wsl-img-package: .wsl-img-run
	docker export ${PACKAGE_NAME}-instance | gzip -c > ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@du -hs ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@$(MAKE .wsl-img-instance-cleanup)

.PHONY: .wsl-img-run
.wsl-img-run: .wsl-img-instance-cleanup
	docker run --name ${PACKAGE_NAME}-instance ${DOCKER_IMAGE}

.PHONY: .wsl-img-instance-cleanup
.wsl-img-instance-cleanup:
	@docker stop ${DOCKER_IMAGE}-instance 1>/dev/null 2>/dev/null || true
	@docker rm ${DOCKER_IMAGE}-instance 1>/dev/null 2>/dev/null || true
